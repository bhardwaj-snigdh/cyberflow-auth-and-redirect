import './Sidebar.css';
import { Col } from 'reactstrap';

export default function Sidebar({ xs, md }) {
  return (
    <Col xs={xs} md={md} className="Sidebar">
      <h3 className="Sidebar__Heading">Website Add-ons</h3>
      <p>Check out our easy to integrate Web Site Add-ons!</p>
      <ul className="Sidebar__List">
        <li>Smart Survey</li>
        <li>Smart Multi Poll</li>
        <li>Smart Poll</li>
        <li>Smart Guest Book</li>
        <li>Smart Quote</li>
      </ul>
    </Col>
  );
}
