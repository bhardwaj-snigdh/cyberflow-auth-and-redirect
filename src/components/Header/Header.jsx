import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import {
  Collapse,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Jumbotron,
  Nav,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  NavItem,
  NavLink,
} from 'reactstrap';
import auth from '../../auth';

import sunflowerImage1 from '../../assets/sunflower-image-1.jpg';
import sunflowerImage2 from '../../assets/sunflower-image-2.jpg';
import womanImage from '../../assets/woman.jpg';

import './Header.css';

export default function Header() {
  const history = useHistory();
  const [isNavOpen, setIsNavOpen] = useState(false);
  const [isDropdownOpen, setIsDropdownOpen] = useState(false);

  return (
    <header className="Header">
      <Navbar dark expand="md" className="Header__NavBar">
        <NavbarBrand className="Header__NavBrand">Brand Name</NavbarBrand>
        <NavbarToggler
          className="Header__NavToggler"
          onClick={() => setIsNavOpen((isOpened) => !isOpened)}
        />
        <Collapse className="Header__NavCollapse" isOpen={isNavOpen} navbar>
          <Nav className="Header__Nav" navbar>
            <NavItem>
              <NavLink href="#">Home</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="#">About Us</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="#">Articles</NavLink>
            </NavItem>
            <Dropdown
              nav
              isOpen={isDropdownOpen}
              toggle={() => setIsDropdownOpen((isOpen) => !isOpen)}
            >
              <DropdownToggle nav>Photos</DropdownToggle>
              <DropdownMenu className="Header__DropdownMenu">
                <DropdownItem className="Header__DropdownItem">
                  Photo 1
                </DropdownItem>
                <DropdownItem className="Header__DropdownItem">
                  Photo 2
                </DropdownItem>
                <DropdownItem className="Header__DropdownItem">
                  Photo 3
                </DropdownItem>
              </DropdownMenu>
            </Dropdown>
            <NavItem>
              <NavLink
                href="/"
                onClick={(e) => {
                  e.preventDefault();
                  auth.logout();
                  history.push('/login');
                }}
              >
                Logout
              </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
      <Jumbotron className="Header__Text">
        <h1>Your site name</h1>
        <p>Your slogan comes here</p>
      </Jumbotron>
      <div className="Header__ImageContainer">
        <img className="Header__Image" src={sunflowerImage1} alt="" />
        <img className="Header__Image" src={womanImage} alt="" />
        <img className="Header__Image" src={sunflowerImage2} alt="" />
      </div>
    </header>
  );
}
