import { Redirect, Route } from 'react-router-dom';
import auth from '../auth';

export default function ProtectedRoute({ component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={(props) => {
        if (auth.isLoggedIn()) return <Component {...props} />;
        else return <Redirect to="/signup" />;
      }}
    />
  );
}
