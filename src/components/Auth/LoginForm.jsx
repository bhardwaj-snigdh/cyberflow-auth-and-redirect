import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import {
  Button,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Label,
  Spinner,
} from 'reactstrap';

import authenticateAndSaveToken, {
  authTypes,
} from './authenticateAndSaveToken';

export default function LoginForm({ setAlertMessage, setIsAlertVisible }) {
  const history = useHistory();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  return (
    <div className="Form">
      <h2>Login</h2>
      <p className="mb-5">Welcome! Please enter our account details.</p>
      <Form
        onSubmit={(e) => {
          e.preventDefault();
          setIsLoading(true);
          authenticateAndSaveToken(authTypes.LOGIN, { email, password })
            .then(() => {
              setIsLoading(false);
              history.push('/home');
            })
            .catch((err) => {
              setIsLoading(false);
              console.error(err);
              setAlertMessage(err.message);
              setIsAlertVisible(true);
            });
        }}
      >
        <FormGroup className="mb-3">
          <Label htmlFor="email">Email Address</Label>
          <InputGroup className="Form__InputGroup">
            <Input
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              className="Form__Input"
              type="email"
              id="email"
            />
            <InputGroupAddon addonType="append">
              <InputGroupText className="Form__InputIcon">
                <i className="bi bi-person-lines-fill" />
              </InputGroupText>
            </InputGroupAddon>
          </InputGroup>
        </FormGroup>
        <FormGroup className="mb-3">
          <Label htmlFor="password">Password</Label>
          <InputGroup className="Form__InputGroup">
            <Input
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              className="Form__Input"
              type="password"
              id="password"
            />
            <InputGroupAddon addonType="append">
              <InputGroupText className="Form__InputIcon">
                <i className="bi bi-arrow-repeat" />
              </InputGroupText>
            </InputGroupAddon>
          </InputGroup>
        </FormGroup>
        <Button className="Form__Btn" color="primary">
          Register {isLoading && <Spinner size="sm" />}
        </Button>
      </Form>
    </div>
  );
}
