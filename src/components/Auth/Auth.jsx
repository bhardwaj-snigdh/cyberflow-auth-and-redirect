import { Col } from 'reactstrap';
import { Link, useLocation } from 'react-router-dom';
import SignupForm from './SignupForm';
import LoginForm from './LoginForm';

import './Auth.css';

export default function Auth({ md, xs, setAlertMessage, setIsAlertVisible }) {
  const location = useLocation();
  const isLoggingIn = location.pathname === '/login';

  return (
    <Col className="FormContainer" md={md} xs={xs}>
      <span className="FormContainer__Link">
        {isLoggingIn ? (
          <>
            Don't have an account? <Link to="/signup">Sign Up</Link>
          </>
        ) : (
          <>
            Already have an account? <Link to="/login">Login</Link>
          </>
        )}
      </span>
      {isLoggingIn ? (
        <LoginForm
          setAlertMessage={setAlertMessage}
          setIsAlertVisible={setIsAlertVisible}
        />
      ) : (
        <SignupForm
          setAlertMessage={setAlertMessage}
          setIsAlertVisible={setIsAlertVisible}
        />
      )}
    </Col>
  );
}
