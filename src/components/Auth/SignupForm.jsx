import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import {
  Button,
  Col,
  Form,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Label,
  Row,
  Spinner,
} from 'reactstrap';

import authenticateAndSaveToken, {
  authTypes,
} from './authenticateAndSaveToken';

export default function SignupForm({ setAlertMessage, setIsAlertVisible }) {
  const history = useHistory();
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [mobileNumber, setMobileNumber] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  return (
    <div className="Form">
      <h2>Register</h2>
      <p className="mb-5">Welcome! Please enter our account details.</p>
      <Form
        onSubmit={(e) => {
          e.preventDefault();
          setIsLoading(true);
          authenticateAndSaveToken(authTypes.SIGNUP, {
            firstName,
            lastName,
            email,
            password,
            mobileNumber,
          })
            .then(() => {
              setIsLoading(false);
              history.push('/home');
            })
            .catch((err) => {
              setIsLoading(false);
              console.error(err);
              setAlertMessage(err.message);
              setIsAlertVisible(true);
            });
        }}
      >
        <Row className="Form__Row">
          <Col md={6}>
            <FormGroup className="mb-3">
              <Label htmlFor="firstName">First Name</Label>
              <Input
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                className="Form__Input"
                type="text"
                id="firstName"
              />
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup className="mb-3">
              <Label htmlFor="lastName">Last Name</Label>
              <Input
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
                className="Form__Input"
                type="text"
                id="lastName"
              />
            </FormGroup>
          </Col>
        </Row>
        <FormGroup className="mb-3">
          <Label htmlFor="email">Email Address</Label>
          <InputGroup className="Form__InputGroup">
            <Input
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              className="Form__Input"
              type="email"
              id="email"
            />
            <InputGroupAddon addonType="append">
              <InputGroupText className="Form__InputIcon">
                <i className="bi bi-person-lines-fill" />
              </InputGroupText>
            </InputGroupAddon>
          </InputGroup>
        </FormGroup>
        <FormGroup className="mb-3">
          <Label htmlFor="password">Password</Label>
          <InputGroup className="Form__InputGroup">
            <Input
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              className="Form__Input"
              type="password"
              id="password"
            />
            <InputGroupAddon addonType="append">
              <InputGroupText className="Form__InputIcon">
                <i className="bi bi-arrow-repeat" />
              </InputGroupText>
            </InputGroupAddon>
          </InputGroup>
        </FormGroup>
        <FormGroup className="mb-3">
          <Label htmlFor="mobileNumber">Mobile Phone</Label>
          <InputGroup className="Form__InputGroup">
            <Input
              value={mobileNumber}
              onChange={(e) => setMobileNumber(e.target.value)}
              className="Form__Input"
              type="tel"
              id="mobileNumber"
            />
            <InputGroupAddon addonType="append">
              <InputGroupText className="Form__InputIcon">
                <i className="bi bi-telephone" />
              </InputGroupText>
            </InputGroupAddon>
          </InputGroup>
        </FormGroup>
        <Button className="Form__Btn" color="primary">
          Register {isLoading && <Spinner size="sm" />}
        </Button>
      </Form>
    </div>
  );
}
