import auth from '../../auth';

export const authTypes = {
  LOGIN: 'login',
  SIGNUP: 'signup',
};

export default async function authenticateAndSaveToken(type, payload) {
  let res = null;
  try {
    res = await fetch(`https://restlogin.herokuapp.com/${type}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
      },
      body: new URLSearchParams(payload),
    });
  } catch (e) {
    throw new Error('Unable to fetch, please check your connection.');
  }

  const text = await res.text();
  let status = null;
  let accessToken = null;
  try {
    const data = JSON.parse(text);
    if (type === authTypes.SIGNUP) {
      ({ status, accessToken } = data);
    } else {
      ({ status, accessToken } = data.result);
    }
  } catch (err) {
    throw new Error(text);
  }

  if (status === true && accessToken) {
    auth.login(accessToken);
  } else {
    throw new Error('Some internal error occurred');
  }
}
