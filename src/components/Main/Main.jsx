import { Col } from 'reactstrap';
import './Main.css';

export default function Main({ xs, md }) {
  return (
    <Col xs={xs} md={md} className="Main">
      <h2 className="Main__Heading">
        Features of this CSS Personal/General Template
      </h2>
      <ul className="Main__List">
        <li>
          Attractive general or personal website design using bright colors
        </li>
        <li>Optimized, fast-loading and W3c certified valid CSS & HTML code</li>
        <li>Easy-to-edit text Links with easy-to-use Drop-down Menus</li>
        <li>
          Table-less layout CSS design. All <strong>11</strong> linked HTML
          pages included
        </li>
        <li>
          Cross Browser Compatible -{' '}
          <em>Tested for IE 5+, Netscape 6+, Opera 7+, Firefox 1.0+</em>
        </li>
        <li>
          Design to stretch and fit all resolutions (800 x 600 and higher)
        </li>
      </ul>
      <p>
        <em>Buy Now</em> for Only <strong>$59.95</strong>!{' '}
        <a href="/">Add to Cart</a>
      </p>
      <p>
        Smart Webby offers professional web templates at affordable prices.
        Choose from a variety of high quality websites designs to find the
        perfect template for your line of business.{' '}
        <a rel="noreferrer" href="http://www.smartwebby.com" target="_blank">
          More Templates from SmartWebby.com
        </a>
      </p>
    </Col>
  );
}
