import jwtDecode from 'jwt-decode';

class Auth {
  isLoggedIn() {
    try {
      const token = localStorage.getItem('token');
      if (!token) {
        console.log('No token found');
        return false;
      }

      const { exp } = jwtDecode(token);
      if (exp < new Date() / 1000) {
        console.log('Token has expired');
        return false;
      }

      console.log('User logged in');
      return true;
    } catch (e) {
      console.error('Invalid JWT Token', e);
      return false;
    }
  }

  login(token) {
    try {
      jwtDecode(token);
      localStorage.setItem('token', token);
      return true;
    } catch (e) {
      console.error('Ill-formed Token: Not saving ', token);
      return false;
    }
  }

  logout() {
    localStorage.removeItem('token');
  }
}

export default new Auth();
