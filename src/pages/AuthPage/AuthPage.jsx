import { useEffect, useState } from 'react';
import { Alert, Container, Row } from 'reactstrap';
import Banner from '../../components/Banner';
import Auth from '../../components/Auth';

import './AuthPage.css';

function AuthPage() {
  const [alertMessage, setAlertMessage] = useState('');
  const [isAlertVisible, setIsAlertVisible] = useState(false);

  useEffect(() => {
    if (!isAlertVisible) return;

    const timer = setTimeout(() => {
      setIsAlertVisible(false);
    }, 3000);

    return () => {
      clearTimeout(timer);
    };
  }, [isAlertVisible]);

  return (
    <Container fluid className="Auth">
      <Row>
        <Alert
          className="Auth__Alert"
          color="warning"
          isOpen={isAlertVisible}
          toggle={() => setIsAlertVisible((isVisible) => !isVisible)}
        >
          {alertMessage}
        </Alert>
        <Banner md={4} xs={12} />
        <Auth
          setAlertMessage={setAlertMessage}
          setIsAlertVisible={setIsAlertVisible}
          md={8}
          xs={12}
        />
      </Row>
    </Container>
  );
}

export default AuthPage;
