import { Link } from 'react-router-dom';

import notFoundImage from '../../assets/404-error.png';
import './NotFound.css';

export default function NotFound() {
  return (
    <div className="NotFound">
      <img className="NotFound__Image" src={notFoundImage} alt="Page not found." />
      <h2 className="NotFound__Text">Page not found</h2>
      <Link to="/home" className="NotFound__Link">Back to home</Link>
    </div>
  )
}