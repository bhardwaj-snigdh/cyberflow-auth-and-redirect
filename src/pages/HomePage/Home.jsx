import { Container, Row } from 'reactstrap';
import Header from '../../components/Header';
import Sidebar from '../../components/Sidebar';
import Main from '../../components/Main';
import Footer from '../../components/Footer';

import './Home.css';

function Home() {
  return (
    <div className="Home">
      <Header />
      <Container className="Home__Container" fluid>
        <Row className="Home__ContainerRow">
          <Sidebar md={3} xs={12} />
          <Main md={9} xs={12} />
        </Row>
      </Container>
      <Footer />
    </div>
  );
}

export default Home;